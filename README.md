[//]: # "Formatação Markdown: https://help.github.com/en/articles/basic-writing-and-formatting-syntax"

# MedLevensohn | Gestão de Licitações

Esse é o projeto para criação de um sistema de gestão de licitações para a empresa Prospect Medlevensohn, utilizando interface **THF** da [TOTVS](https://thf.totvs.com.br/home).


## 📝 Informações do projeto
- MedLevensohn trabalha com distribuição de produtos, equipamentos no ramo da saúde.
- Utilizam atualmente outro _ERP_* que já não atende a necessidade.
- Utilizam _software web_** que gerencia licitações a que concorre a empresa.
	- Software avisa como ocorreu a licitação
	- Fornecimentos de leilão ganho podem ser "empenhado" parcialmente, por periodo. Ou seja, entrega dos produtos podem ser parciais, até o total.
- Opção de uso do módulo Protheus CRM para administração de vendas.
- Opção de uso do fluxo de processos do Fluig
- Discutido a possibilidade de implementar com THF, consumindo REST Protheus.

[//]: # " *  ERP TARGET http://www.targetsistemas.com.br/"
[//]: # " ** Mannesoft Winner https://www.mannesoftwinner.com.br/"


## Fases de projeto
[//]: # "Icones 📝✅"
[//]: # " [ ] **Fase X (mm/aaaa):** Lorem ipsum dolor sit amet... "
- [ ] **Fase 1 (06/2019):** Estratégia de desenvolver um protótipo para apresentação ao futuro cliente.


## Executando o projeto

1. Baixar os arquivos da aplicação e colar em uma pasta separada para o projeto, por exemplo: _C:\projeto-licitacoes_
2. Abrir pasta do projeto com um editor de código de preferência _(VSCode, Sublime, Atom, etc.)_
3. Instalar as dependências executando o comando abaixo na pasta do projeto (via terminal, prompt, ou terminal integrado do editor):
```
npm install
```
4. Após instaladas todas as dependências, executar o comando abaixo, e manter o terminal aberto:
```
ng serve
```
5. Abrir a url `http://localhost:4200` no browser _(Chrome, Mozila, Edge)_. Os arquivos de código podem ser editados normalmente, que a aplicação atualizará no browser a cada salvamento nos arquivos.

**Obs: Funciona parcialmente no Internet Explorer com versões 9 e 10**.
