import { Component } from '@angular/core';
import { ThfMenuItem } from '@totvs/thf-ui';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  menuItemSelected: string = 'none';

  /** define o logotipo a ser exibido no menu */
  readonly logotipo: string = 'assets/logo-medlevensohn.png';
  /** Variavel que define os itens a serem exibidos no menu lateral */
  readonly menus: Array<ThfMenuItem> = [
    {
      label: 'Dashboard',
      link: '/home',
      icon: 'thf-icon-chart-area'
    },
    {
      label: 'Licitações',
      link: '/licitacoes',
      icon: 'thf-icon-document-double',
      subItems: [
        { label: 'Lista', action: this.printMenuAction, link: 'licitacoes/lista' },
        { label: 'Novo', action: this.printMenuAction, link: 'licitacoes/novo' }
      ],
    },
  ];

  printMenuAction(menu: ThfMenuItem) {
    this.menuItemSelected = menu.label;
    // alert(`Selecionado ${menu.label}`);
  }

}
