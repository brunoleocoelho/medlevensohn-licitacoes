import { ThfTableColumn } from '@totvs/thf-ui';
import json from '../../data/licitacoes-data.json';
// const json = require('../../data/licitacoes-data.json');

const filtro = (search: string, dados?: any[]) => {
    const data = dados || [];
    if (search) {
        const licitacoes: Array<any> = data.length && data;
        const filtrado = licitacoes.filter(
            item => {
                return item.objeto ? item.objeto.includes(search) : false;
            }
        );
        return filtrado;
    }
    return null;
};

const loadData = (ultimoItem) => {
    const first = ultimoItem - 10;
    const last = ultimoItem > json.licitacoes.length ? json.licitacoes.length : ultimoItem;
    const data: Array<any> = [];

    for (let index = first; index < last; index++) {
        data.push(json.licitacoes[index]);
    }
    console.log('data', first, last, data);
    return data;
};

/** Retorna modelo a ser usado pela lista de licitação */
const licitacaoData = {

    /** Retorna dados de exemplo da API licitações, com paginação de itens */
    dataLicitacoes: (ultimoItem) => {
        return loadData(ultimoItem);
    },

    /** Retorna os dados filtrados, conforme range inicial*/
    filtraPor: (search: string, rangeEnd: number) => {
        const licitacoes = loadData(rangeEnd);
        return filtro(search, licitacoes);
    },

    /** Retorna as colunas a serem usadas na lista de licitação */
    colunasLicitacoes: (): Array<ThfTableColumn> => ([
        {
            property: 'identificador',
            label: 'Id.Licitação',
            tooltip: 'Identificador da Licitação',
            type: 'string',
            width: '15%'
        },
        // {
        //     property: 'numero_processo',
        //     label: 'Nº Processo',
        //     tooltip: 'Número do Processo',
        //     type: 'string',
        //     width: '10%'
        // },
        // {
        //     property: 'data_abertura_proposta',
        //     label: 'Abert.da proposta',
        //     tooltip: 'Data de abertura da proposta',
        //     type: 'date'
        // },
        // {
        //     property: 'data_entrega_edital',
        //     label: 'Entrega Edital',
        //     tooltip: 'Data de Entrega do Edital',
        //     type: 'dateTime'
        // },
        // {
        //     property: 'data_entrega_proposta',
        //     label: 'Entrega da proposta',
        //     tooltip: 'Data de entrega da proposta',
        //     type: 'dateTime'
        // },
        {
            property: 'data_publicacao',
            label: 'Publicação',
            tooltip: 'Data da publicação da licitação',
            type: 'date',
            width: '10%'
        },
        // {
        //     property: 'endereco_entrega_edital',
        //     label: 'End.Entrega Edital',
        //     tooltip: 'Endereço de Entrega do Edital',
        //     type: 'string'
        // },
        {
            property: 'objeto',
            label: 'Obj.da Licitação',
            tooltip: 'Objeto da Licitação',
            type: 'string',
            width: '25%'
        },
        {
            property: 'nome_responsavel',
            label: 'Respons.',
            tooltip: 'Nome do Responsável pela Licitação',
            type: 'string',
            width: '10%'
        },
        // {
        //     property: 'funcao_responsavel',
        //     label: 'Função',
        //     tooltip: 'Função do Responsável pela Licitação',
        //     type: 'string'
        // },
        // {
        //     property: 'informacoes_gerais',
        //     label: 'Inf.Gerais',
        //     tooltip: 'Informações Gerais',
        //     type: 'string'
        // },
        // {
        //     property: '_links.modalidade_licitacao.title',
        //     label: 'Modalidade',
        //     tooltip: 'Modalidade da Licitação',
        //     type: 'number'
        // },
        {
            property: 'numero_aviso',
            label: 'Nº Aviso',
            tooltip: 'Número do Aviso da Licitação',
            type: 'number',
            width: '7%'
        },
        {
            property: 'numero_itens',
            label: 'Itens',
            tooltip: 'Número de Itens',
            type: 'number',
            width: '5%'
        },
        {
            property: 'situacao_aviso',
            label: 'Sit.aviso',
            tooltip: 'Situação do aviso',
            labels: [
                { value: 'Publicado', color: 'success', label: 'Public.', tooltip: 'Publicado' },
                { value: 'Divulgado', color: 'warning', label: 'Divulg.', tooltip: 'Divulgado' }
            ],
            type: 'label',
            width: '7%'
        },
        {
            property: 'tipo_pregao',
            label: 'Pregão',
            tooltip: 'Tipo do Pregão',
            subtitles: [
                { value: 'Presencial', color: 'warning', content: 'P', label: 'Presencial' },
                { value: 'Eletrônico', color: 'success', content: 'E', label: 'Eletrônico' }
            ],
            type: 'subtitle',
            width: '7%'
        },
        {
            property: 'tipo_recurso',
            label: 'Recurso',
            tooltip: 'Tipo do Recurso',
            labels: [
                { value: 'Nacional', color: 'success', label: 'N', tooltip: 'Nacional' }
            ],
            type: 'label',
            width: '7%'
        },
        {
            property: 'uasg',
            label: 'UASG',
            tooltip: 'Código da UASG',
            type: 'number',
            width: '7%'
        },
    ]),
};

export default licitacaoData;
