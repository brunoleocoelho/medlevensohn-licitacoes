import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
// import { Location } from '@angular/common';
// import { HttpClient, HttpParams } from '@angular/common/http';
import { ThfPageAction, ThfBreadcrumb, ThfTableColumn, ThfPageFilter, ThfTableComponent } from '@totvs/thf-ui';
import { Router } from '@angular/router';
import licitacaoData from './licitacoes-lista.data';


@Component({
  selector: 'app-licitacoes-lista',
  templateUrl: './licitacoes-lista.component.html',
  styleUrls: ['./licitacoes-lista.component.css']
})
export class LicitacoesListaComponent implements OnInit, AfterContentInit {
  private lastToLoad: number;
  private listHeight: number;
  private searchTerm: string;
  private items: Array<Object> = [];
  private colunas: Array<ThfTableColumn> = [];
  private selectedItem: any;
  private isLoading = false;
  private isEditingEnable: Boolean;

  @ViewChild(ThfTableComponent) thfTable: ThfTableComponent;

  public readonly breadcrumb: ThfBreadcrumb = {
    items: [
      { label: 'Licitações', link: '/' },
      { label: 'Lista' }
    ]
  };
  public readonly actions: Array<ThfPageAction> = [
    { label: 'Incluir', action: () => this.goNovaLicitacao(), icon: 'thf-icon-plus' },
    { label: 'Editar', action: () => this.editLicitacao(), icon: 'thf-icon-edit', disabled: () => !this.isEditingEnable },
  ];

  public readonly filter: ThfPageFilter = {
    action: () => this.filterData(this.searchTerm.trim()),
    ngModel: 'searchTerm',
    placeholder: 'Pesquisar por...'
  };

  /** CONSTRUTOR DA CLASSE */
  constructor(private router: Router) {
    this.lastToLoad = 10;
    this.listHeight = 0;
  }

  ngOnInit() {
    this.loadLicitacoes();
    this.resizeListHeight();
    this.isEditingEnable = this.thfTable ? this.thfTable.getSelectedRows().length > 0 : false;
  }

  ngAfterContentInit() {
    console.log('ngAfterContentInit entrou');
  }

  /** Carrega as licitações aserem exibidas na lista */
  loadLicitacoes = () => {
    this.isLoading = true;
    setTimeout(
      () => {
        // nomes das colunas
        this.colunas = licitacaoData.colunasLicitacoes().map(item => {
          if (item.property === 'identificador') {
            item.type = 'link';
            item.action = this.editLicitacao;
          }
          return item;
        });
        // dados para exibir
        this.items = licitacaoData.dataLicitacoes(this.lastToLoad);
        this.items.forEach( item => {
          if (item['objeto']) {
            if (item['objeto'].length > 40) {
              item['objeto'] = item['objeto'].slice(0, 40);
            }
          }
        });
        this.items.sort((a, b) => (this.setDateToNumber(a['data_publicacao']) - this.setDateToNumber(b['data_publicacao'])));

        this.isLoading = false;
      },
      250
    );
  }

  /** Faz um resize para a lista de licitações */
  resizeListHeight = () => {
    // workaround to get height of element
    const xPageContent = document.getElementsByClassName('thf-page-content');
    const elmt = xPageContent.item(0);
    this.listHeight = +elmt['style']['height'].slice(0, 2);
    console.log('this.listHeight', this.listHeight);
  }

  /** Método p/ navegar a página de inclusão de licitação */
  private goNovaLicitacao = () => {
    this.router.navigateByUrl('/licitacoes/novo');
  }

  /** Abre a edição da licitação recebida por parametro */
  private editLicitacao = () => {
    // this.router.navigateByUrl('/licitacoes/edit/'+ numProc);
    console.log(this.selectedItem);
  }

  /** Carrega mais 10 itens para a lista */
  public loadMore = () => {
    this.isLoading = true;
    this.lastToLoad += 10;
    const novosItens = licitacaoData.dataLicitacoes(this.lastToLoad);
    this.items.push(...novosItens);
    this.isLoading = false;
    this.items.sort((a, b) => (this.setDateToNumber(a['data_publicacao']) - this.setDateToNumber(b['data_publicacao'])));
  }

  /** Efetua um filtro rápido nos itens da lista */
  private filterData = (termo: string) => {
    this.isLoading = true;
    this.lastToLoad = this.lastToLoad > 10 ? this.lastToLoad : 10;
    if (termo.length > 0) {
      // this.items = this.items.filter(item => item['objeto'].includes(termo) );
      this.items = licitacaoData.filtraPor(termo, this.lastToLoad);
    } else {
      this.loadLicitacoes();
    }
    this.isLoading = false;
  }

  /** Habilita o botão de edição */
  public toggleEditing = (row: any) => {
    const selectedItems = this.thfTable.getSelectedRows();
    if (selectedItems.length > 0) {
      this.selectedItem = row;
      this.isEditingEnable = true;
    } else {
      this.isEditingEnable = false;
    }
    // console.log('row', row);
    // console.log('selectedItems', selectedItems);
  }

  /** Transforma a data yyy-mm-dd em Number */
  private setDateToNumber = (strDate: String) => {
    const parts = strDate ? strDate.split('-') : [];
    const joined = parts.join('') || '';
    return Number.parseInt(joined);
  }
}
