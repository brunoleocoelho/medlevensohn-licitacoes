import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicitacoesListaComponent } from './licitacoes-lista.component';

describe('LicitacoesListaComponent', () => {
  let component: LicitacoesListaComponent;
  let fixture: ComponentFixture<LicitacoesListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicitacoesListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicitacoesListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
