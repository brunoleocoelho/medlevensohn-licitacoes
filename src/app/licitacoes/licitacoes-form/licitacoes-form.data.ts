
import jsonFields from './modelo-form-licitacoes.json';

const licitacoesFormData = {

    /** Retorna o modelo com os campos de formulário de licitações */
    getFormFields: () => {
        return jsonFields;
    }
};

export default licitacoesFormData;
