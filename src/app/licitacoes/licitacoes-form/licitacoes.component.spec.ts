import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ThfPageModule } from '@totvs/thf-ui';

import { LicitacoesComponent } from './licitacoes.component';

describe('LicitacoesComponent', () => {
  let component: LicitacoesComponent;
  let fixture: ComponentFixture<LicitacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicitacoesComponent ],
      imports: [
        RouterTestingModule.withRoutes([]),
        ThfPageModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicitacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
