import { Component, OnInit } from '@angular/core';
import { ThfDynamicFormField, ThfPageAction, ThfBreadcrumb } from '@totvs/thf-ui';
import licitacoesFormData from './licitacoes-form.data';

@Component({
  selector: 'app-home',
  templateUrl: './licitacoes.component.html',
  styleUrls: ['./licitacoes.component.css']
})
export class LicitacoesComponent implements OnInit {
  public titulo = 'Nova Licitação';
  public fields: Array<ThfDynamicFormField>;
  public licitacaoData: Array<any>;

  public readonly breadcrumb: ThfBreadcrumb = {
    items: [
      { label: 'Licitações', link: '/' },
      { label: 'Novo' }
    ]
  };
  public readonly actions: Array<ThfPageAction> = [
    { label: 'Salvar', action: () => alert('clicou em Salvar'), icon: 'thf-icon-plus' },
    { label: 'Cancelar', action: () => alert('clicou em Cancelar'), icon: 'thf-icon-edit', type: 'danger'  },
  ];

  constructor() { }

  ngOnInit() {
    this.fields = licitacoesFormData.getFormFields();
  }

}
