
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LicitacoesComponent } from './licitacoes-form/licitacoes.component';
import { LicitacoesListaComponent } from './licitacoes-lista/licitacoes-lista.component';


/** Este Routes controla as rotas dentro deste modulo de licitações */
export const LicitacoesRoutes: Routes = [
  {
    path: 'lista',
    component: LicitacoesListaComponent
  },
  {
    path: 'novo',
    component: LicitacoesComponent
  },
  {
    path: '',
    component: LicitacoesListaComponent
  },
];


@NgModule({
  imports: [
    RouterModule.forChild(LicitacoesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class LicitacoesRoutingModule { }
