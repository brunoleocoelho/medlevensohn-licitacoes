import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module'; // importa módulo do THF

import { LicitacoesRoutingModule } from './licitacoes-routing.module';
import { LicitacoesComponent } from './licitacoes-form/licitacoes.component';
import { LicitacoesListaComponent } from './licitacoes-lista/licitacoes-lista.component';

@NgModule({
  declarations: [
    LicitacoesComponent,
    LicitacoesListaComponent
  ],
  imports: [
    SharedModule,
    LicitacoesRoutingModule
  ]
})
export class LicitacoesModule { }
