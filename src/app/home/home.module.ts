import { NgModule } from '@angular/core';
import { ThfKendoModule } from '@totvs/thf-kendo';
import { SharedModule } from '../shared/shared.module'; // importa módulo do THF

import { HomeRoutingModule } from './home-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    ThfKendoModule,
    SharedModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
