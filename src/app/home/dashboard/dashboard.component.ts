import { Component, OnInit } from '@angular/core';
import { ThfChartType, ThfDialogService, ThfPieChartSeries, ThfBreadcrumb, ThfCheckboxGroupOption } from '@totvs/thf-ui';
import { ThfKendoPieChartSeries, ThfKendoColumnChartSeries, ThfKendoRangeChartSeries } from '@totvs/thf-kendo'
import dashboardData from './dashboard.data';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  //chartType: ThfChartType = ThfChartType.Pie;
  public readonly breadcrumb: ThfBreadcrumb = {
    items: [
      { label: 'Home', link: '/' },
      { label: 'Dashboard' }
    ]
  };

  public readonly situacaoAviso: Array<ThfPieChartSeries> = dashboardData.getTipoSituacoes();
  // [
  //   { category: 'Finland', value: 9.6, tooltip: 'Finland (Europe)' },
  //   { category: 'Norway', value: 7.2, tooltip: 'Norway (Europe)' },
  //   { category: 'Netherlands', value: 6.7, tooltip: 'Netherlands (Europe)' },
  //   { category: 'Slovenia', value: 6.1, tooltip: 'Slovenia (Europe)' },
  //   { category: 'Austria', value: 5.5, tooltip: 'Austria (Europe)' }
  // ];

  // /** Opção utilizando gráfico Kendo Pie */
  // public readonly pregoes: Array<ThfKendoPieChartSeries> =  [{ 
  //   data: [
  //     { category: 'Presencial', value: 22  },
  //     { category: 'Eletrônico', value: 170 }
  //   ]}
  // ];
  public readonly pregoes: Array<ThfPieChartSeries> =  [
    { category: 'Presencial', value: 22, tooltip: undefined },
    { category: 'Eletrônico', value: 170, tooltip: undefined }
  ];

  public readonly showLabels = false;
  public readonly conquistas: Array<ThfKendoColumnChartSeries> = [
    { data: this.genColumnData(), name: 'Licitações Municipais' },
    { data: this.genColumnData(), name: 'Licitações Estaduais' },
    { data: this.genColumnData(), name: 'Licitações Federais' },
  ]

  public readonly rangeBarData: Array<ThfKendoRangeChartSeries> = [
    { data: this.genRangeData(), name: 'Lic.#1' },
    { data: this.genRangeData(), name: 'Lic.#2' },
  ]


  constructor() { }

  ngOnInit() {
  }

  /** Dados aleatórios para Kendo Column Chart */
  private genColumnData():Array<number> {
    let maxValue = 75000
    let numbers: Array<number> = [];
    for (let index = 0; index < 10; index++) {
      const randNum = this.genNumber(maxValue);
      numbers.push(randNum);
    }
    return numbers;
  }

  /** Dados aleatórios para o Kendo Range Bar Chart */
  private genRangeData(): Array<Array<number>> {
    let start = 1;
    let end = 3;
    let coords: Array<Array<number>> = [];

    for (let index = 0; index < 10; index++) {
      const x = parseInt((Math.random() * (start * index)).toString(), 10);
      const y = parseInt((Math.random() * (end * index)).toString(), 10);
      const value = (x < y) ? [x,y] : [y,x];
      coords.push(value);
    }
    console.log(coords);
    return coords;
  }

  /** Para gerar um número aleatórios do tipo float */
  private genNumber(maxValue: number): number {
    return parseFloat((Math.random() * maxValue).toFixed(2));
  }
}
