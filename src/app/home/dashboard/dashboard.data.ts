import jsonData from '../../data/licitacoes-data.json';

/** Transforma a data yyy-mm-dd em Number */
const setDateToNumber = (strDate: string) => {
    const parts = strDate ? strDate.split('-') : [];
    const joined = parts.join('') || '';
    return Number.parseInt(joined);
};

const dashboardData = {

    /** Retorna os tipos de situações de aviso dos ultimos 12 meses */
    getTipoSituacoes: () => {
        const arra: Array<any> = jsonData.licitacoes;
        const situacoes = arra.filter(val => {
            // const dtPublic = setDateToNumber(val['data_publicacao']);
            // const umAnoAtras =  setDateToNumber( new Date(
            //     new Date().getFullYear() - 1,
            //     new Date().getMonth(),
            //     new Date().getDate()).toISOString().slice(0, 10)
            // );
            return (
                ['Publicado', 'Divulgado'].includes(val['situacao_aviso'])
                // && dtPublic > umAnoAtras
            ) && val ;
        });

        const filtered = situacoes.reduce((prev, curr) => {
            const chave = curr['situacao_aviso'];
            return { ...prev, [chave]: [...(prev[chave] || []), curr] };
        }, {});

        // { category: 'Publicado', value: 2796, tooltip: 'Brazil (South America)' }
        const sections = Object.keys(filtered).map(item => ({
            category: item,
            value: filtered[item].length,
            tooltip: undefined
        }));

        return sections;
    }
};

export default dashboardData;
