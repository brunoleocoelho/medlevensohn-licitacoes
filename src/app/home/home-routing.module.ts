import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

/** Este Routes controla as rotas dentro deste modulo HOME */
export const homeRoutes: Routes = [
  {
    path: '/home',
    component: DashboardComponent
  },
  {
    path: '',
    component: DashboardComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
