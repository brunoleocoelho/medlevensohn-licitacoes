import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/** Rotas para os módulos internos da aplicação */
const routes: Routes = [
  {
    path: 'licitacoes',
    loadChildren: './licitacoes/licitacoes.module#LicitacoesModule'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

/** Este modulo exporta as rotas a serem usadas internamente na aplicação */
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
